type RGB = `rgb(${number}, ${number}, ${number})` | `rgb(${number},${number},${number})`;
type RGBA = `rgba(${number}, ${number}, ${number}, ${number})` | `rgba(${number},${number},${number},${number})`;
type HEX = `#${string}`;
type Color = RGB | RGBA | HEX | string;

interface ColorCode {
    name: string;
    code: Color;
}

interface Activity extends ActivityPreset{
    id: string;
    name: string;
    endHour: number;
    backgroundColor: Color;
    color: Color;
}

interface ActivityPreset {
    day: string;
    startHour: number;
}

export type {
    Activity,
    ActivityPreset,
    ColorCode,
    Color
};
