import React, { FC } from 'react';
import style from './Home.module.css';
import { Link } from "react-router-dom";

interface HomeProps {

}

const Home: FC<HomeProps> = () => (
  <div>
    <h1 className={style.homeHeader}>Scheduler</h1>
      <div className={'buttonBox ' + style.buttonBox}>
          <Link to={"/creator"}>
              <button>
                  Create schedule
              </button>
          </Link>
      </div>
  </div>
);

export default Home;
