import React, { FC } from 'react';
import styles from './CreatorButtons.module.css';
import { Activity } from "../../../interfaces";
import { isScheduleValid } from "../../../modules/file-helper";

interface CreatorButtonsProps {
    schedule: Activity[];
    isSavingActive: boolean;
    onChangeIsSavingActive: () => void;
    onSetIsActivityModalOpened: () => void;
    onImportSchedule: (schedule: Activity[]) => void;
}

const CreatorButtons: FC<CreatorButtonsProps> = (props) => {


    const handleExportJson = () => {
        const scheduleJson = { schedule: props.schedule };
        const stringifyJson = JSON.stringify(scheduleJson, null, "\t");
        const encodedURIJson = encodeURIComponent(stringifyJson);
        const jsonString = `data:text/json;charset=utf-8,${encodedURIJson}`;
        const downloadElement =  document.createElement("a");

        downloadElement.href = jsonString;
        downloadElement.download = "schedule.json";
        downloadElement.click();
    };

    const handleImportJson = () => {
        const importElement = document.createElement("input");
        importElement.type = "file";
        importElement.accept = "application/json";
        importElement.onchange = handleOnChangeImportFile;
        importElement.click();
    };

    const handleOnChangeImportFile = (event: Event) => {
        const files = (event.target as HTMLInputElement).files;
        if (files) {
            const reader = new FileReader();
            reader.onload = handleOnLoadImportFile;
            reader.readAsText(files[0]);
        }
    };

    const handleOnLoadImportFile = (event: ProgressEvent<FileReader>) => {
        const result = event.target ? event.target.result as string : null;
        const parsedResult = result ? JSON.parse(result) : null;
        const schedule = parsedResult && parsedResult.schedule ? parsedResult.schedule : [];
        if (isScheduleValid(schedule)) {
            props.onImportSchedule(schedule);
        }
    };

    return (
        <div className={styles.CreatorButtons}>
            <div id={styles.cookiesCheckBox}>
                <input type="checkbox" id="schedule-cookies"
                       checked={props.isSavingActive}
                       onChange={props.onChangeIsSavingActive}/>
                <label htmlFor="schedule-cookies">Enable <b>SAVING</b> schedule via <b>COOKIES</b></label>
            </div>
            <div className={'buttonBox ' + styles.buttonBox}>
                <button onClick={props.onSetIsActivityModalOpened}>Add activity</button><br/>
                <button onClick={handleImportJson}>Import schedule</button>
                <button onClick={handleExportJson}>Export schedule</button>
            </div>
        </div>
    );
};

export default CreatorButtons;
