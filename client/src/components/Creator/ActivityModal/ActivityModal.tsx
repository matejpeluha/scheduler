import React, { FC } from 'react';
import ReactDom from 'react-dom';
import styles from './ActivityModal.module.css';
import { FaRegWindowClose } from 'react-icons/fa';
import { Activity, ActivityPreset } from "../../../interfaces";
import ActivityForm from "./ActivityForm/ActivityForm";


interface ActivityModalProps {
    isOpen: boolean;
    activity?: Activity;
    activityPreset?: ActivityPreset;
    onCloseModal: () => void;
    onCreateActivity: (activity: Activity) => void;
    onUpdateActivity: (activity: Activity) => void;
    onDeleteActivity: (activityId: string) => void;
}

const ActivityModal: FC<ActivityModalProps> = (props) => props.isOpen ? ReactDom.createPortal(
  <main className={styles.ActivityModal}>
    <div className={styles.activitySetter}>
        <div><FaRegWindowClose onClick={props.onCloseModal} className={styles.closeIcon}/></div>
        <ActivityForm activity={props.activity}
                      activityPreset={props.activityPreset}
                      onCreateActivity={props.onCreateActivity}
                      onUpdateActivity={props.onUpdateActivity}
                      onDeleteActivity={props.onDeleteActivity} />
    </div>
  </main>,
document.body) : null;

export default ActivityModal;
