import React, { FC, useState } from "react";
import styles from './ActivityForm.module.css';
import { Activity, ActivityPreset, Color } from "../../../../interfaces";
import { v4 as generateUuid } from "uuid";
import { getColors, getEndHours, getStartHours, getWeekDays } from "../../../../modules/schedule-helper";

interface ActivityFormProps {
    activity?: Activity;
    activityPreset?: ActivityPreset;
    onCreateActivity: (activity: Activity) => void;
    onUpdateActivity: (activity: Activity) => void;
    onDeleteActivity: (activityId: string) => void;
}

const ActivityForm: FC<ActivityFormProps> = ({ activity, activityPreset, onCreateActivity, onUpdateActivity, onDeleteActivity }) => {
    const [ activityName, setActivityName ] = useState<string>(activity ? activity.name : "");
    const [ day, setDay ] = useState<string>(activity ? activity.day : (activityPreset ? activityPreset.day : getWeekDays()[0]));
    const [ startHour, setStartHour ] = useState<number>(activity ? activity.startHour : (activityPreset ? activityPreset.startHour : getStartHours()[0]));
    const [ endHour, setEndHour ] = useState<number>(activity ? activity.endHour : getEndHours(startHour)[0]);
    const [ backgroundColor, setBackgroundColor ] = useState<Color>(activity ? activity.backgroundColor : getColors()[0].code);
    const [ color, setColor ] = useState<Color>(activity ? activity.color : getColors()[1].code);

    const handleOnStartHourChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const startHour: number = parseInt(event.target.value);
        setStartHour(startHour);
        setEndHour(startHour + 1);
    };

    const handleOnCreateActivity = (): void => {
        const newActivity = { id: generateUuid(), name: activityName, day, startHour, endHour, backgroundColor, color };
        onCreateActivity(newActivity);
    };

    const handleOnUpdateActivity = (): void => {
        const id = activity ? activity.id : generateUuid();
        const updatedActivity: Activity = { id, name: activityName, day, startHour, endHour, backgroundColor, color };
        onUpdateActivity(updatedActivity);
    };

    const handleOnDeleteActivity = (): void => {
        const id = activity ? activity.id : generateUuid();
        onDeleteActivity(id);
    };

    return (
        <div className={styles.ActivityForm}>
            <h1>Activity</h1>
            <div className={styles.inputContainer}>
                <label htmlFor="nameInput">Activity name</label>
                <input id={styles.nameInput} type="text" value={activityName}
                       onChange={(e) => setActivityName(e.target.value)}/>
            </div>
            <div className={styles.inputContainer}>
                <label htmlFor="daySelect">Day</label>
                <select id={styles.daySelect} value={day} onChange={(e) => setDay(e.target.value)}>
                    {getWeekDays().map((day, i) => <option key={i} value={day}>{day}</option>)}
                </select>
            </div>
            <div className={styles.inputContainer}>
                <label htmlFor="startHourSelect">Start hour</label>
                <select id={styles.startHourSelect} value={startHour}
                        onChange={handleOnStartHourChange}>
                    {getStartHours().map((hour, i) => <option key={i} value={hour}>{hour}</option>)}
                </select>
            </div>
            <div className={styles.inputContainer}>
                <label htmlFor="endHourSelect">End hour</label>
                <select id={styles.endHourSelect} value={endHour}
                        onChange={(e) => setEndHour(parseInt(e.target.value))}>
                    {getEndHours(startHour).map((hour, i) => <option key={i} value={hour}>{hour}{hour === 0 ? " (next day)" : ""}</option>)}
                </select>
            </div>
            <div className={styles.inputContainer}>
                <label htmlFor="backgroundColorSelect">Background color</label>
                <select id={styles.backgroundColorSelect} value={backgroundColor}
                        onChange={(e) => setBackgroundColor(e.target.value)}>
                    {getColors().map((color, i) => <option key={i} value={color.code}>{color.name}</option>)}
                </select>
            </div>
            <div className={styles.inputContainer}>
                <label htmlFor="colorSelect">Text color</label>
                <select id={styles.colorSelect} value={color}
                        onChange={(e) => setColor(e.target.value)}>
                    {getColors().map((color, i) => <option key={i} value={color.code}>{color.name}</option>)}
                </select>
            </div>
            <div className={"buttonBox " + styles.buttonBox}>
                {activity ? null : <button onClick={handleOnCreateActivity}>Create</button>}
                {activity ? <button onClick={handleOnUpdateActivity}>Update</button> : null}
                {activity ? <button onClick={handleOnDeleteActivity}>Delete</button> : null}
            </div>
        </div>
    );
};



export default ActivityForm;
