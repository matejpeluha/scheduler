import React, { FC, useState } from 'react';
import styles from './Creator.module.css';
import Schedule from "./Schedule/Schedule";
import { Activity, ActivityPreset } from "../../interfaces";
import ActivityModal from "./ActivityModal/ActivityModal";
import { useCookies } from "react-cookie";
import CreatorButtons from "./CreatorButtons/CreatorButtons";

interface CreatorProps {

}

const Creator: FC<CreatorProps> = () => {
    const [ cookies, setCookie, removeCookie ] = useCookies();
    const [ isSavingActive, setIsSavingActive ] = useState<boolean>(cookies.isSavingActive === "true");
    const [ isActivityModalOpen, setIsActivityModalOpened ] = useState<boolean>(false);
    const [ updatedActivity, setUpdatedActivity ] = useState<Activity | undefined>(undefined);
    const [ updatedActivityPreset, setUpdatedActivityPreset ] = useState<ActivityPreset | undefined>(undefined);
    const [ schedule, setSchedule ] = useState<Activity[]>(cookies.isSavingActive && cookies.schedule ? cookies.schedule : []);


    const handleOnCloseModal = () => {
        setUpdatedActivity(undefined);
        setUpdatedActivityPreset(undefined);
        setIsActivityModalOpened(false);
    };

    const handleOnCreateActivity = (newActivity: Activity): void => {
        const newSchedule = [ ...schedule, newActivity ];
        setSchedule(newSchedule);
        setUpdatedActivity(undefined);
        setUpdatedActivityPreset(undefined);
        setIsActivityModalOpened(false);
        if (isSavingActive) {
            setCookie("schedule", newSchedule);
        }
    };

    const handleOnStartUpdateActivity = (activity: Activity) => {
        setUpdatedActivity(activity);
        setIsActivityModalOpened(true);
    };

    const handleOnUpdateActivity = (changedActivity: Activity) => {
        const newSchedule = schedule.filter((activity) => activity.id !== changedActivity.id);
        newSchedule.push(changedActivity);
        setSchedule(newSchedule);
        setUpdatedActivity(undefined);
        setIsActivityModalOpened(false);
        if (isSavingActive) {
            setCookie("schedule", newSchedule);
        }
    };

    const handleOnDeleteActivity = (activityId: string) => {
        const newSchedule = schedule.filter((activity) => activity.id !== activityId);
        setSchedule(newSchedule);
        setUpdatedActivity(undefined);
        setIsActivityModalOpened(false);
        if (isSavingActive) {
            setCookie("schedule", newSchedule);
        }
    };

    const handleOnStartCreatePresetActivity = (activityPreset: ActivityPreset) => {
        setUpdatedActivityPreset(activityPreset);
        setIsActivityModalOpened(true);
    };

    const handleOnChangeIsSavingActive = () => {
        setIsSavingActive(!isSavingActive);
        setCookie("isSavingActive", !isSavingActive);
        if (!isSavingActive) {
            setCookie("schedule", schedule);
        } else {
            removeCookie("schedule");
        }
    };

    const handleOnImportSchedule = (schedule: Activity[]) => {
        console.log("IMPORTED SCHEDULE: ", schedule);
        setSchedule(schedule);
        if (isSavingActive) {
            setCookie("schedule", schedule);
        }
    };

    return (
        <div id={styles.creator}>
            <ActivityModal isOpen={isActivityModalOpen}
                           activity={updatedActivity}
                           activityPreset={updatedActivityPreset}
                           onCloseModal={handleOnCloseModal}
                           onCreateActivity={handleOnCreateActivity}
                           onUpdateActivity={handleOnUpdateActivity}
                           onDeleteActivity={handleOnDeleteActivity} />
            <Schedule activities={schedule}
                      onStartUpdateActivity={handleOnStartUpdateActivity}
                      onStartCreatePresetActivity={handleOnStartCreatePresetActivity}/>
            <CreatorButtons schedule={schedule}
                            isSavingActive={isSavingActive}
                            onChangeIsSavingActive={handleOnChangeIsSavingActive}
                            onSetIsActivityModalOpened={() => setIsActivityModalOpened(true)}
                            onImportSchedule={handleOnImportSchedule}/>
        </div>
    );
};



export default Creator;
