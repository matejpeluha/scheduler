import React, { FC, CSSProperties, useState } from "react";
import style from './Schedule.module.css';
import { Activity, ActivityPreset } from "../../../interfaces";
import { FaPlusCircle } from 'react-icons/fa';
import { getWeekDays, getStartHours } from "../../../modules/schedule-helper";

interface ScheduleProps {
    activities: Activity[];
    onStartUpdateActivity: (activity: Activity) => void;
    onStartCreatePresetActivity: (activityPreset: ActivityPreset) => void;
}

const Schedule: FC<ScheduleProps> = ({ activities, onStartUpdateActivity, onStartCreatePresetActivity }) => {
    const [ keyMouseEntered, setKeyMouseEntered ] = useState<number | undefined>(undefined);

    const getTableBody = (activities: Activity[]) => {
        return getWeekDays().map((day: string, i: number) => {
            const todayActivities = activities.filter(activity => activity.day === day);
            return (
                <tr key={i}>
                    <th>{day}</th>
                    {getScheduleRow(todayActivities, i )}
                </tr>
            );
        });
    };

    const getScheduleRow = (todayActivities: Activity[], row: number) => {
        let skipCount = 0;
        return getStartHours().map((hour:number, i: number) => {
            if (skipCount > 0) {
                skipCount--;
                return null;
            }

            const activity = todayActivities.find(activity => activity.startHour <= hour && (activity.endHour > hour || activity.endHour === 0));

            if (!activity) {
                const activityPreset: ActivityPreset = { day: getWeekDays()[row], startHour: i };
                i = i + 24 * row;
                return <td key={i}
                           onMouseEnter={() => setKeyMouseEntered(i)}
                           onMouseLeave={() => setKeyMouseEntered(undefined)}
                           onClick={() => onStartCreatePresetActivity(activityPreset)}>
                                {i === keyMouseEntered ? <FaPlusCircle /> : "-"}
                        </td>;
            }

            const endHour = activity.endHour > 0 ? activity.endHour : 24;
            const hourDifference = endHour  - activity.startHour;
            skipCount = hourDifference - 1;
            const columnStyle: CSSProperties = { backgroundColor: activity.backgroundColor, color: activity.color };

            return <td key={i}
                       colSpan={hourDifference}
                       className={style.scheduledActivity}
                       style={columnStyle}
                       onClick={() => onStartUpdateActivity(activity)}>{ activity.name }</td>;
        });
    };

    return (
        <div id={style.tableContainer}>
            <table>
                <thead>
                    <tr>
                        <th>Day/Hour</th>
                        { getStartHours().map((hour: number, i: number) => <th key={i}>{hour}</th>) }
                    </tr>
                </thead>
                <tbody>
                    { getTableBody(activities) }
                </tbody>
            </table>
        </div>
    );
};



export default Schedule;
