import { ColorCode } from "../interfaces";

const getWeekDays = (): string[] => {
    return [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    ];
};

const getStartHours = (): number[] => {
    return [ ...Array(24).keys() ];
};

const getEndHours = (startHour: number): number[] => {
    const endHours = getStartHours().filter((hour) => hour > startHour);
    endHours.push(0);
    return endHours;
};

const getColors = (): ColorCode[] => {
    return [
        { name: "white", code: "#FFFFFF" },
        { name: "black", code: "#000000" },
        { name: "red", code: "#FF0000" },
        { name: "yellow", code: "#FFFF00" },
        { name: "green", code: "#008000" },
        { name: "blue", code: "#0000FF" },
        { name: "purple", code: "#800080" },
        { name: "pink", code: "#FFC0CB" },
        { name: "orange", code: "#FFA500" },
    ];
};


export {
    getWeekDays,
    getStartHours,
    getEndHours,
    getColors
};
