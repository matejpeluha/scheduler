import { getColors, getEndHours, getStartHours, getWeekDays } from "./schedule-helper";

const isScheduleValid = (schedule: []): boolean => {
    const activityKeys = [ "id", "name", "day", "startHour", "endHour", "backgroundColor", "color" ];
    for (const activity of schedule) {
        if ( typeof activity !== "object"){
            return false;
        }
        if (areArraysDifferent(Object.keys(activity), activityKeys)) {
            return false;
        }
        if (typeof activity["id"] !== "string"
            || typeof activity["name"] !== "string"
            || !getWeekDays().includes(activity["day"])
            || !getStartHours().includes(activity["startHour"])
            || !getEndHours(activity["startHour"]).includes(activity["endHour"])
            || (getColors().filter(c => c.code === activity["backgroundColor"])).length !== 1
            || (getColors().filter(c => c.code === activity["color"])).length !== 1){
            return false;
        }
    }

    return true;
};

const areArraysDifferent = (array1: string[], array2: string[]): boolean => {
    const difference1 = array1.filter(x => !array2.includes(x));
    const difference2 = array2.filter(x => !array1.includes(x));

    return difference1.length > 0 && difference2.length > 0;
};

export {
    isScheduleValid
};
